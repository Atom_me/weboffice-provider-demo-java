# WebOffice 开放平台文档接入回调示例

## 依赖

JDK 13+

## 使用

假设您使用的是 Linux 系统，并且安装好了 JDK 13 和 Maven

1. 准备好要使用的文档，存放在一个目录内，例如 `/data/docs`
2. 准备一个能从公网访问到的 IP 地址或域名，例如 `http://callback.demo.foo`
3. 将上述信息填写到项目中的 `src/main/resources/application.properties` 中，示例如下：
~~~
...
# weboffice-provider
weboffice.provider.host=http://callback.demo.foo # 用于给开放平台做回调访问
weboffice.provider.docs_dir=/data/docs # 本地文档存储路径
...
~~~

4. 下载 [WebOffice Java SDK](https://gitee.com/wpsweboffice/weboffice-provider-v3-java)
5. 进入 SDK 项目的下载目录，编译出 jar 包，拷贝到 Demo 项目的 `/lib` 目录下：
~~~
$ cd /path/to/sdk
$ mvn package
# 这里采用软链接的方式，构建的 jar 包名字可能因为版本更新而不同 
$ ln -s /path/to/sdk/target/weboffice-provider-v3-0.0.1.jar /path/to/demo/lib/weboffice-provider-v3-0.0.1.jar
~~~

6. 编译、启动服务，可以直接在 IDE 中运行，或通过 maven 构建出 jar 后执行
7. 项目启动后，会加载配置的目录下的文件(文件内容会另存一份到临时目录，后续修改也修改临时目录下的文件，不会影响配置目录下的原始文件)并打印加载的日志如下：
~~~
init demo with file dir: /data/docs, service host: http://solution-demo.wps.cn
init with file: 1, f, 文档1.pdf
init with file: 2, f, 文档2.docx
init with file: 3, s, 文档3.xlsx
init docs done.
~~~
以其中 `init with file: 1, f, 文档1.pdf` 为例，`1` 即为文件 id，`f` 为 WebOffice 需要的文档类型，之后请求 WebOffice 预览该文档时要用到
8. 在 WebOffice 开放平台-控制台，配置回调域名、接口等。接口配置好之后，最好分别验证一下
9. 通过步骤5.项目启动后日中中打印出来的文件 id、文档类型信息，请求 WebOffice 开放平台打开文档，在 Demo 中，只生成了 id 分别为 `1` 和 `2` 的两个用户，且用户 id 即为其 `token`
示例：
~~~
// 设置 token
jssdk.setToken('1');

// URL 中的 type 和 id 即日志中打印的 WebOffice 文档类型和 id
https://o.wpsgo.com/office/{office_type}/{file_id}?_w_appid={your_app_id}&_w_tokentype=1
~~~

## 说明

WebOffice Java SDK 中需要接入方实现的接口有：

- ExtendCapacityService # 扩展能力接口，包括历史版本、重命名等功能
- MultiPhaseFileStorageService # 文档三阶段保存接口，非必须，且与 SinglePhaseFileStorageService 互斥，实现一个即可
- PreviewService # 预览服务接口，必须实现，包括获取文档信息、下载地址、当前用户权限接口
- SinglePhaseFileStorageService # 文档保存接口，非必须，且与 MultiPhaseFileStorageService 互斥，实现一个即可
- UserService # 获取用户信息，非必须

Demo 中全部实现了，实际项目中可以按需实现。

更多文档请参考：

[WebOffice 开放平台-回调配置](https://solution.wps.cn/docs/callback/summary.html)

[WebOffice Java SDK](https://gitee.com/wpsweboffice/weboffice-provider-v3-java)
