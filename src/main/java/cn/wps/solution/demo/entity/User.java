package cn.wps.solution.demo.entity;

import cn.wps.solution.weboffice.provider.v3.model.UserInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_user")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String nickname;

    private String avatar;

    public UserInfo toUserInfo() {
        return UserInfo.builder()
                .id(String.valueOf(this.id))
                .name(this.nickname)
                .avatarUrl(this.avatar)
                .build();
    }
}
