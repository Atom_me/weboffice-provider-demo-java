package cn.wps.solution.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Builder
@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class FileId implements Serializable {
    private String id;
    private int version = 1;

    public FileId copyForNewVersion() {
        return new FileId(id, version + 1);
    }
}
