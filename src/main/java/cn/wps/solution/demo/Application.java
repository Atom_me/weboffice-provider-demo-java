package cn.wps.solution.demo;

import cn.wps.solution.demo.entity.File;
import cn.wps.solution.demo.entity.FileId;
import cn.wps.solution.demo.entity.User;
import cn.wps.solution.demo.repository.FileRepository;
import cn.wps.solution.demo.repository.StorageRepository;
import cn.wps.solution.demo.repository.UserRepository;
import cn.wps.solution.demo.util.FileUtils;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.stream.Stream;

@Log4j2
@SpringBootApplication(scanBasePackages = {"cn.wps.solution.weboffice.provider.v3", "cn.wps.solution.demo"})
public class Application implements ApplicationRunner {
    @Autowired
    ApplicationContext context;
    @Value("${weboffice.provider.host:http://localhost:8080}")
    private String host;
    @Value("${weboffice.provider.docs_dir:/data/docs}")
    private String dir;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FileRepository fileRepository;
    @Autowired
    private StorageRepository storageRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    @SneakyThrows
    public void run(ApplicationArguments args) {
        log.info("init demo with file dir: {}, service host: {}", dir, host);

        // init user
        final User joeDoe = this.userRepository.save(User.builder()
                .id(1L)
                .nickname("JoeDoe")
                .avatar("")
                .build());

        final User janeDoe = this.userRepository.save(User.builder()
                .id(2L)
                .nickname("JaneDoe")
                .avatar("")
                .build());

        // init files
        try (Stream<Path> files = Files.walk(Paths.get(dir))) {
            files.filter(Files::isRegularFile)
                    .filter(FileUtils::support)
                    .forEach(f -> {
                        final File file = fileRepository.save(File.builder()
                                .id(FileId.builder().id(FileUtils.generateId()).version(1).build())
                                .name(f.getFileName().toString())
                                .size(FileUtils.size(f))
                                .createTime(LocalDateTime.now())
                                .modifyTime(LocalDateTime.now())
                                .creator(joeDoe)
                                .modifier(janeDoe)
                                .build());
                        storageRepository.save(file.getId(), FileUtils.readAllBytes(f));
                        log.info("init with file: {}, {}, {}", file.getId().getId(), FileUtils.officeType(f), file.getName());
                    });
        }

        log.info("init docs done.");
    }
}